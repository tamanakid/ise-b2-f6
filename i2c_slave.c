#include "LPC17xx.h"
#include "GPIO_LPC17xx.h"
#include "PIN_LPC17xx.h"
#include "Driver_I2C.h"

#include "HTTP_Server.h"



/* Definitions */

ARM_DRIVER_I2C *i2c_drv = &Driver_I2C2;



static volatile uint32_t I2C_Event;
/**
 * Thread: I2C Slave
 */
void thread_i2c (void const *arg) {
  i2c_slave_initialize();
  while (1) {
    i2c_slave_listen ();
  }
}



/* I2C Functions */

/* I2C Signal Event function callback */
static void I2C_SignalEvent (uint32_t event) {
  I2C_Event |= event;
}


void i2c_slave_initialize () {
  /* Initialize I2C peripheral */
  i2c_drv->Initialize(I2C_SignalEvent); /*I2C_SignalEvent*/
 
  /* Power-on I2C peripheral */
  i2c_drv->PowerControl(ARM_POWER_FULL);
 
  /* Configure I2C bus */
  i2c_drv->Control(ARM_I2C_OWN_ADDRESS, I2C_SLAVE_ADDR);
}


void i2c_slave_listen () {
  uint8_t buffer_rx[4];
  
  /* Receive chunk */
  i2c_drv->SlaveReceive(buffer_rx, 4);
  while ((I2C_Event & ARM_I2C_EVENT_TRANSFER_DONE) == 0);
  /* Clear transfer done flag */
  I2C_Event &= ~ARM_I2C_EVENT_TRANSFER_DONE;
	
	set_latency_measures(buffer_rx);
}
