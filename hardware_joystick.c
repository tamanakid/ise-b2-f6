#include "GPIO_LPC17xx.h"
#include "PIN_LPC17xx.h"

#include "rtc.h"

#include "HTTP_Server.h"



/* Definitions */


/**
 * Configure the Joystick to trigger interrupts upon center-key press
 */
void joystick_initialize(void) {
	PIN_Configure(PORT_JST, PIN_JST_C, PIN_FUNC_0, PIN_PINMODE_PULLDOWN, PIN_PINMODE_NORMAL);
	PIN_Configure(PORT_JST, PIN_JST_D, PIN_FUNC_0, PIN_PINMODE_PULLDOWN, PIN_PINMODE_NORMAL);
	PIN_Configure(PORT_JST, PIN_JST_L, PIN_FUNC_0, PIN_PINMODE_PULLDOWN, PIN_PINMODE_NORMAL);
	PIN_Configure(PORT_JST, PIN_JST_R, PIN_FUNC_0, PIN_PINMODE_PULLDOWN, PIN_PINMODE_NORMAL);

	LPC_GPIOINT->IO0IntEnR |= (1UL << PIN_JST_D);
	LPC_GPIOINT->IO0IntEnR |= (1UL << PIN_JST_C);
	LPC_GPIOINT->IO0IntEnR |= (1UL << PIN_JST_L);
	LPC_GPIOINT->IO0IntEnR |= (1UL << PIN_JST_R);

	NVIC_EnableIRQ(EINT3_IRQn);
	
	/* I2C nRst */
	PIN_Configure(PORT_RGB, PIN_RED, PIN_FUNC_0, PIN_PINMODE_PULLUP, PIN_PINMODE_NORMAL);
	GPIO_PinWrite(PORT_RGB, PIN_RED, 1);
	osDelay(1);
}



/**
 * GPIO Interrupt Service Routine (ISR)
 */
void EINT3_IRQHandler() {
	uint32_t statusR = LPC_GPIOINT->IO0IntStatR;
	
	/* (JST Center): Resets time to default date */
	if (statusR & (1 << PIN_JST_C)) {
		LPC_GPIOINT->IO0IntClr |= (1 << PIN_JST_C);
		rtc_reset_full_time();
  }
	
	/* (JST Down): interrupt latency measurement */
	else if (statusR & (1 << PIN_JST_D)) {
    LPC_GPIOINT->IO0IntClr |= (1 << PIN_JST_D);
		GPIO_PinWrite (PORT_RGB, PIN_GREEN, 0);
		hard_delay(10);
		GPIO_PinWrite (PORT_RGB, PIN_GREEN, 1);
  }
	
	/* (JST Left): Reduce LCD write interval */
	if (statusR & (1 << PIN_JST_L)) {
		LPC_GPIOINT->IO0IntClr |= (1 << PIN_JST_L);
		lcd_write_interval = lcd_write_interval == 1 ? 1 : lcd_write_interval - 1;
  }
	
	/* (JST Right): Increase LCD write interval */
	if (statusR & (1 << PIN_JST_R)) {
		LPC_GPIOINT->IO0IntClr |= (1 << PIN_JST_R);
		lcd_write_interval = lcd_write_interval == 6 ? 6 : lcd_write_interval + 1;
  }
}
