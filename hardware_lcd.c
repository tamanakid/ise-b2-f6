#include <string.h>
#include <stdio.h>

#include "lcd.h"

#include "HTTP_Server.h"



/* Definitions */

int lcd_write_interval = 4;

uint8_t latency_max_8bits[4];


/* Latency Registering */

float latency_min = 0;
float latency_max = 0;
float latency_avg = 0;
int latency_measures = 0;



/**
 * Registers latency measurements
 */
void set_latency_measures (uint8_t* measure_8bits) {
	static int lcd_write_count = 15;
	
	float measure = ((measure_8bits[0]*256 + measure_8bits[1])*256 + measure_8bits[2])*256 + measure_8bits[3];
	
	/**
	 * Convert counted time into real latency in microseconds
	 * PinWrite execution time: 0.280 us
	 * Median int_res synchronization delay: 0.025 us (Due to metastability prevention)
	 * Total non-latency delay: 305 ns
	 */
	measure = measure / 100;
	measure = measure - 0.305;
	
	if (measure > 0 && measure < 249990) {
		latency_measures++;
		
		if (latency_min == 0 || latency_min > measure) {
			latency_min = measure;
		}
		
		if (latency_max == 0 || latency_max < measure) {
			latency_max = measure;
			osSignalSet(id_thread_flash, OS_EVENT_FLASH_WRITE);
		}
		
		latency_avg = ((latency_measures - 1)*latency_avg + measure)/latency_measures;
	}
	
	/* Check for LCD write period (4 seconds ) */
	lcd_write_count++;
	if (lcd_write_count >= lcd_write_interval*4) {
	  lcd_write();
		lcd_write_count = 0;
	}
}



/**
 * Configure the LCD and initialize to clear values
 */
void lcd_initialize(void) {
	init_lcd();
  reset_lcd();
}


/**
 * Write values read from the cgi POST request to the LCD
 */
void lcd_write(void) {
	char lcd_buf[24+1];
	static bool show_write_interval = true;
	strcpy(lcd_buf, CLEAR_STRING);
	
	sprintf(lcd_buf, "Min:%.2f  Max:%.2f", latency_min, latency_max);
	escribe_frase_L1(lcd_buf, sizeof(lcd_buf));
	
	if (show_write_interval) {
		sprintf(lcd_buf, "Avg:%.2f  Interval: %d", latency_avg, lcd_write_interval);
		show_write_interval = false;
	} else {
		sprintf(lcd_buf, "Avg:%.2f  ISR:%d", latency_avg, latency_measures);
		show_write_interval = true;
	}
	escribe_frase_L2(lcd_buf, sizeof(lcd_buf));
	
	copy_to_lcd();
}
